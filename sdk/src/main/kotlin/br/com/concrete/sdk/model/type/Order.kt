package br.com.concrete.sdk.model.type

import android.support.annotation.StringDef

@StringDef(ASCENDING, DESCENDING)
@Retention(AnnotationRetention.SOURCE)
annotation class Order