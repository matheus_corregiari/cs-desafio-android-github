package br.com.concrete.sdk.model.type

import android.support.annotation.StringDef

@StringDef(STARS, FORKS, UPDATED)
@Retention(AnnotationRetention.SOURCE)
annotation class SortRepo