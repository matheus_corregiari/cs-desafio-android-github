package br.com.concrete.sdk.model.type

import android.support.annotation.StringDef

@StringDef(CREATED, UPDATED, POPULATITY, LONG_RUNNING)
@Retention(AnnotationRetention.SOURCE)
annotation class SortPullRequest