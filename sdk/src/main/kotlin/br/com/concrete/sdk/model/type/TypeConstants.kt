package br.com.concrete.sdk.model.type

//region Sort Pull Request | Repo
const val UPDATED = "updated"
//endregion

//region Sort PullRequest
const val CREATED = "created"
const val POPULATITY = "popularity"
const val LONG_RUNNING = "long-running"
//endregion

//region Sort Repo
const val STARS = "stars"
const val FORKS = "forks"
//endregion

//region State
const val OPEN = "open"
const val CLOSED = "closed"
const val ALL = "all"
//endregion

//region Order
const val ASCENDING = "asc"
const val DESCENDING = "desc"
//endregion