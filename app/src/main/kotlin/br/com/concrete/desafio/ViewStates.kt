@file:JvmName("ViewStates")
package br.com.concrete.desafio

internal const val LIST_STATE = 0
internal const val LOADING_STATE = 1
internal const val EMPTY_STATE = 2
internal const val ERROR_STATE = 3