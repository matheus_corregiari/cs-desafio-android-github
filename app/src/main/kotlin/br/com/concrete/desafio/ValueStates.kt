@file:JvmName("ValueStates")

package br.com.concrete.desafio

internal const val STATE_MACHINE = "STATE_MACHINE"
internal const val STATE_ADAPTER = "STATE_ADAPTER"
const val STATE_MACHINE_CURRENT_KEY = "StateMachine.CurrentKey"